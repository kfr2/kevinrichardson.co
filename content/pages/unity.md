Title: Game Development Resources  
Date: 2018-02-24 14:36  
Slug: game-dev  
Author: Kevin Richardson  
Summary:  A collection of game development resources I've found useful.  

I'm starting to look into game development as a hobby and figured it would be useful
to chronicle the resources I find useful.

### Unity
* [2D UFO Tutorial](https://unity3d.com/learn/tutorials/s/2d-ufo-tutorial) -- I
  found this tutorial to be exceptionally approachable for a beginner to both game
  development and the Unity engine. It has a very soup-to-nuts approach that starts
  at the beginning of setting up a project in the engine and ends with a fully-working
  (albeit somewhat basic) game.
* [Learn to Code by Making Games - Complete C# Unity Developer](https://www.udemy.com/unitycourse/learn/v4/overview) --
  This course provides many dozens of hours of content and guides the student through
  making a variety of games, starting with a very basic console-based number guessing
  game and ending with a pirate-themed real-time strategy game. Although I'm not too
  far into it yet, I really appreciate the instructor's patient explanations and find
  that any given lecture is designed to be digestible in around ten minutes. I may add
  more to this short review as I progress further through the course.


(Last updated 2018-02-24)
