Title: What I'm doing now  
Date: 2015-05-01 12:07  
Slug: now  
Author: Kevin Richardson  
Summary:  A quick summary of Kevin Richardson and kevinrichardson.co.  

Hi, I'm Kevin!

For work, I'm currently helping teams in an educational company with deploying
their services into various Kubernetes clusters as part of their CI/CD pipelines.
I'm also working to create on-demand development environments so the teams no
longer have to share one dev environment and hope that changes made by other teams
don't impact their own. In the not-so-distant past, my focus was on developing,
monitoring, and scaling primarily Python-based Dockerized microservices to support
single-page applications as well as building the underlying infrastructure on
which the services are hosted.

For personal amusement, I'm looking into the [Go programming language](https://golang.org)
and acquiring an even deeper understanding of distributed systems.

### Contact
You can email me at kevin [at] kevinrichardson.co or contact me via Twitter at [_kfr2](https://twitter.com/_kfr2).

(Last updated 2018-10-10. See [Chris Winter's "now" page](http://cwinters.com/now/) for the inspiration behind this page.)
