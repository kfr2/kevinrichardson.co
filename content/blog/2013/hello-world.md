Title: Hello World  
Date: 2013-01-01 19:55  
Tags: meta, github, writing, python  
Slug: hello-world  
Author: Kevin Richardson  
Summary: Wherein the new kevinrichardson.co, a personal website powered by Pelican, is introduced by its creator.  

### Prologue
Good day to you, dear reader!  Welcome to kevinrichardson.co, the personal website of [Kevin Richardson](http://github.com/kfr2), a software engineer from the lovely Pittsburgh, Pennsylvania. Herein I will write about various things I find interesting as well as any revelations I have while developing different pieces of software or user experiences. Although I currently work predominantly as a web developer (mostly with the lovely [Django](http://djangoproject.com) framework), my personal and business work touches upon many facets and technologies. Thus, my posts will not be strictly related to web development but may be very exploratory in nature.

### The technology behind this site
This site is powered by [Pelican](http://blog.getpelican.com/) -- a static website generator -- and is hosted on Amazon S3. Content is written in [Markdown](http://daringfireball.net/projects/markdown/), a lightweight markup language that allows one to easily add syntax like headings, anchors, and lists to an article. I use OS X in conjunction with [Sublime Text](www.sublimetext.com) (and occasionally the venerable [vim](www.vim.org)) to develop and write the various content contained on this site.

If you desire your own personal site, I highly recommend a similar setup as the workflow for adding content to this site is quite easy if you are familiar with GitHub and Python. If you're not (and desire to setup a similar website), fret not -- I will soon write a post describing how one can setup her own installation of Pelican with GitHub pages.

### Epilogue
Thank you very much for perusing my small corner of the web. If you already haven't, you may be interested in viewing the projects I maintain on [my GitHub account](https://github.com/kfr2). Furthermore, please feel free to contact me using the methods listed in [my humans.txt file](/humans.txt) as I would love to discuss Life, the Universe, and Everything with you.

*Pax et bonum: peace and good.*
