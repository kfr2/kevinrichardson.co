Title: Continuous Deployment to AWS with Ansible
Date: 2014-06-17 21:13  
Tags: ansible, aws, devops, continuous-deployment
Slug: continuous-deployment-to-aws-with-ansible
Author: Kevin Richardson  
Summary: A discussion of one approach to continuously deploying code changes to AWS with Ansible.
Status: draft

