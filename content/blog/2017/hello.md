Title: Hello 2017 
Date: 2017-05-13 15:58  
Tags: miscellaneous  
Slug: hello-2017  
Author: Kevin Richardson  
Summary: Merely checking in.  

Hi!  

It's been quite some time since my last post but I suppose that's what happens when life, the universe, and everything occurs. I'm currently modifying this project through [Cloud9](https://c9.io)'s online IDE which is pretty convenient as it provides both a text editor as well as an environment in which to run various kinds of tasks. I've imported the source of this site from [its Gitlab repository](https://gitlab.com/kfr2/kevinrichardson.co) into a Cloud9 project. This allows me to modify the various files and then have the site build and upload itself through Gitlab's continuous integration pipeline. All-in-all, it's very simple but extraordinarily handy for the minimal amount of effort I want to put into maintaining a personal website!  

A large amount of my recent work has revolved around revamping my team's development flow by replacing our homespun rake solution with docker-compose. So far, it seems like it's been a very good change but time will illuminate any issues with the approach. In addition, I've begun to somewhat more seriously look into learning the Go programming language so I can expand my horizons (as well as potentially stave off some boredom). I may or may not write more about these things in the future.  

Until later, take care.  