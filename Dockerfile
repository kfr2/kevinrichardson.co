FROM python:2-onbuild
MAINTAINER Kevin Richardson <https://github.com/kfr2>

ENV PYTHONDONTWRITEBYTECODE=1

# This port is exposed for dev server use (dev_site.sh)
EXPOSE 8000

