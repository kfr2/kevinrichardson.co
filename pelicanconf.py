#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals


SITENAME = u'Kevin Richardson'
TAGLINE = u'turning frying pans into drying pans'
FOOTER_MESSAGE = u'This work is licensed under the <a href="http://creativecommons.org/licenses/by-sa/3.0/" rel="license">CC BY-SA</a>.'  # noqa
SITEURL = ''
FEED_DOMAIN = SITEURL
FEED_RSS = 'feeds/all.rss'
TIMEZONE = 'America/New_York'
DEFAULT_LANG = u'en'
DATE_FORMATS = {
    'en': '%Y-%m-%d',
}
DEFAULT_PAGINATION = 1

THEME = 'themes/pelican-foundation-redux'

PAGE_SAVE_AS = ('{slug}/index.html')
ARTICLE_URL = ('articles/{date:%Y}/{date:%m}/{date:%d}/{slug}/')
ARTICLE_SAVE_AS = ('articles/{date:%Y}/{date:%m}/{date:%d}/{slug}/index.html')
ABOUT_SAVE_AS = ('now/index.html')
ARCHIVES_SAVE_AS = ('articles/index.html')
MENUITEMS = (
    (u"What I'm doing now", '/now/', 'fa fa-user'),
    (u'all articles', '/articles/', 'fa fa-list'),
)

LINKEDIN_USERNAME = u'kevinfrichardson'
TWITTER_USERNAME = u'_kfr2'
GITHUB_USERNAME = u'kfr2'
GITLAB_USERNAME = u'kfr2'

# Plugins and their settings.
PLUGIN_PATHS = ['plugins']
PLUGINS = ['gist']
