#!/bin/bash
IMAGE='kevinrichardson.co'
NAME='dev-site-builder'
PORT='8000'

function usage ()
{
  echo "Usage:"
  echo "$0 start -- start pelican reload and HTTP server on port $PORT"
  echo "$0 stop -- stop the container that handles the above"
  exit 0
}

function start ()
{
  docker build -t $IMAGE .
  docker run -d --name $NAME -p=$PORT:$PORT -v "$PWD":/usr/src/myapp -w /usr/src/myapp $IMAGE make devserver
  echo "Site should be available at http://0.0.0.0:$PORT"
}

function stop ()
{
  docker stop $NAME && docker rm $NAME
}


if [[ $# -eq 0 ]]; then
  usage
fi

if [ "$1" == "start" ]; then
  start
elif [ "$1" == "stop" ]; then
  stop
else
  usage
fi

