#!/usr/bin/env python
# -*- coding: utf-8 -*- #

import sys
sys.path.append('.')
from pelicanconf import *


GOOGLE_ANALYTICS = "UA-52492610-1"
SITEURL = '//kevinrichardson.co'
FEED_DOMAIN = SITEURL

