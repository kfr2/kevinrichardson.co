IMAGE='kevinrichardson.co'
docker build -t $IMAGE .
docker run -it --rm --name site-builder -v "$PWD":/usr/src/myapp -w /usr/src/myapp $IMAGE make publish
