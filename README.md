# kevinrichardson.co

[ ![Gitlab Build Status for kfr2/kevinrichardson.co](https://gitlab.com/kfr2/kevinrichardson.co/badges/branch/build.svg) ](https://gitlab.com/kfr2/kevinrichardson.co/builds)

This repository contains the source files for [kevinrichardson.co](http://kevinrichardson.co),
the personal website of [Kevin Richardson](https://github.com/kfr2).  [Pelican](https://getpelican.com),
a Python-based static website generator, is used to compile these files into the
website.  The theme for this site is [also available on GitHub](https://github.com/kfr2/pelican-foundation-redux).

## Setup
1. `git clone https://github.com/kfr2/kevinrichardson.co.git ~/my-site`
2. `cd ~/my-site`
3. `git submodule init && git submodule update`
4. Update the various setting files and content as you see fit.
5. From here, there are two (or possibly more) paths to building the site. See
   the *Docker* or *Python* sections for more details.
6. `make upload` may be useful if you use Amazon S3 to host the site.

### Docker
This section assumes Docker is installed on (or available to) the system.

The `build.sh` script will build the site's content into the output directory.
The `dev.sh` script will start or stop a container that runs Pelican in auto-rebuild
mode as well as a SimpleHTTPServer on host port 8000.

### Python
This section assumes Python is installed on the system.

Install the Python requirements (`pip install -r requirements.txt`) into either
your global Python site-packages or a virtualenv (preferred). The following
`make` commands will then prove useful:

* `make html` -- compile a development site into **output/**
* `make devserver` -- run Pelican in auto-rebuild mode as well as a SimpleHTTPServer on port 8000
* `make publish` -- compile a production-ready site into **output/**

## License
These files -- excluding those contained within the `content` directory -- are
licensed under the ISC License.  Please see `LICENSE` for more information.  The
files within the `content` directory are released under the [Creative Commons Attribution-ShareAlike 3.0 Unported License](http://creativecommons.org/licenses/by-sa/3.0/).

