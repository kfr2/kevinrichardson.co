PELICAN=pelican
PELICANOPTS=

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/public
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

help:
	@echo 'Makefile for a pelican Web site                                        '
	@echo '                                                                       '
	@echo 'Usage:                                                                 '
	@echo '   make html                        (re)generate the web site          '
	@echo '   make clean                       remove the generated files         '
	@echo '   make regenerate                  regenerate files upon modification '
	@echo '   make publish                     generate using production settings '
	@echo '   make upload                      upload files in `public/` to S3    '
	@echo '   make gsupload                    upload files in `public/` to Google Storage '
	@echo '   make serve                       serve site at http://localhost:8000'
	@echo '   make devserver                   start a pelican rebuild and dev server on port 8000 '
	@echo '                                                                       '


html: clean $(OUTPUTDIR)/index.html
	@echo 'Done'

$(OUTPUTDIR)/%.html:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
	if test -d $(BASEDIR)/extra; then cp $(BASEDIR)/extra/* $(OUTPUTDIR)/; fi

clean:
	find $(OUTPUTDIR)/ -mindepth 1 -maxdepth 1 ! -name ".gitignore" -exec rm -r -- {} +

regenerate: clean
	$(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

serve:
	cd $(OUTPUTDIR) && python -m SimpleHTTPServer

devserver:
	echo "Starting up Pelican and SimpleHTTPServer"
	cd $(OUTPUTDIR) && python -m SimpleHTTPServer &
	cd $(BASEDIR) && $(PELICAN) --debug --autoreload -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

publish:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)
	if test -d $(BASEDIR)/extra; then cp $(BASEDIR)/extra/* $(OUTPUTDIR)/; fi

upload:
	aws s3 sync public s3://kevinrichardson.co/

gsupload:
	gsutil -m rsync -d -r public gs://kevinrichardson.co/
	gsutil -m acl ch -r -u AllUsers:R gs://kevinrichardson.co/*

.PHONY: html help clean regenerate serve devserver publish github
